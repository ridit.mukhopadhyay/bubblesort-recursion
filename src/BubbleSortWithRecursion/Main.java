package BubbleSortWithRecursion;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {2,1,3,4};
		int n = arr.length;
		displayArray(bubbleSort(n, arr));

	}
	public static int[] bubbleSort(int n,int[] arr) {
		for(int i = 1;i<n;i++) {
			if(arr[i] < arr[i-1]) {
				int temp = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = temp;
			}
		}
		if (n == 1) {
			return arr;
		}
		else {
			return bubbleSort(n-1, arr);

		}
	}
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}
	
	

}
